Source: sitecopy
Section: web
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Homepage: https://www.manyfish.co.uk/sitecopy/
Standards-Version: 4.7.0
Build-Depends: debhelper-compat (= 13),
               gettext,
               libexpat1-dev,
               libneon27-gnutls-dev,
               libxml2-dev,
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/sitecopy
Vcs-Git: https://salsa.debian.org/debian/sitecopy.git

Package: sitecopy
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: program for managing a WWW site via FTP, SFTP, DAV or HTTP
 Sitecopy is for copying locally stored websites to remote servers. With a
 single command, the program will synchronize a set of local files to a
 remote server by performing uploads and remote deletes as required. The
 aim is to remove the hassle of uploading and deleting individual files
 using an FTP client. Sitecopy will also optionally try to spot files you
 move locally, and move them remotely.
 .
 Sitecopy is designed to not care about what is actually on the remote
 server - it simply keeps a record of what it THINKS is in on the remote
 server, and works from that.
